import React from 'react';
import {Link,useParams} from 'react-router-dom'

import '../Styles/Viewer.css'

function setPage(workID,page)
{
    return '/work/' + workID + '/' + page;
}


function clamp(val,min,max) {
    return Math.max(min,Math.min(val,max));
}

function PageNavigation(props) {
   //instead of defining clamp this is a good case to use conditionals
   //and just disable the link outright.
   //but for now.

    //if page is not first
    const canFirst = props.pageNumber != 1;
    //if page can be last.
    const canPrevious = props.pageNumber > 1;
    //if page can be next
    const canNext = props.pageNumber < props.lastPage;

    const canLast = props.pageNumber != props.lastPage;
    
    
    return(
        <div className="pageNavigation">
            {canFirst && <Link to={setPage(props.workID,1)}> &lt;&lt; </Link>}
            {!canFirst && <p className="invalid">&lt;&lt; </p>}
            {canPrevious && <Link to={setPage(props.workID,props.pageNumber-1)}> &lt; </Link>}
            {!canPrevious && <p className="invalid">&lt; </p>}
            <p>{props.pageNumber}</p>
            {canNext && <Link to={setPage(props.workID,props.pageNumber+1)}> &gt;</Link>}
            {!canNext && <p className="invalid"> &gt;</p>}
            {canLast && <Link to={setPage(props.workID,props.lastPage)}> &gt;&gt;</Link>}
            {!canLast && <p className="invalid"> &gt;&gt;</p>}
        </div>
    )
}
function PageDisplay(props) {
    
    const canPrevious = props.pageNumber > 1;    
    const canNext = props.pageNumber < props.lastPage;

    return(
        <div className="pageDisplay">
            
            <img className="page" src={props.page} alt={props.pageAlt}/>
            {canPrevious && <Link to={setPage(props.workID,props.pageNumber-1)}>
                <span className="previousPage"></span>
            </Link>}
            {canNext && <Link  to={setPage(props.workID,props.pageNumber+1)}>
                <span className="nextPage" ></span>
            </Link>}

        </div>
    )
}

export default function PageView(props){
    const {workID,pageNumber} = useParams();

    const idNum = parseInt(pageNumber)
    const lastPage = props.comic.length
    const currentPage = props.comic[idNum-1]
    
    return(
        <div>
            <h1></h1>            
            <PageNavigation pageNumber={idNum} workID ={workID} lastPage={lastPage} />
            <PageDisplay page={currentPage.comicPage} pageNumber={idNum} lastPage={lastPage} workID={workID} pageAlt={currentPage.pageAlt}/> 
            <PageNavigation pageNumber={idNum} workID={workID} lastPage={lastPage}/>
        </div>
    )

}