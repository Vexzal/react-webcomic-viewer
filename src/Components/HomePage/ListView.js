//so this should just take a list of entries and produce a list of comics.
//breakdown is complicated.

import React from 'react';
import ListEntry from './ListEntry'



function listBuild(databaseEntry) {
    return <ListEntry 
    imgSrc={databaseEntry.previewImage}
    title={databaseEntry.title} 
    date={databaseEntry.lastUpdated}
    //this should probably be an href.
    //a link to the work page.
    workLocation={"/work/" + databaseEntry.location + "/1"}/>
}


export default function ListView(props){
    
    

    return (
        <div className="ListView">
            {props.databaseCollection.map(listBuild)}
        </div>
    )
}