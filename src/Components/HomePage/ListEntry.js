import React from 'react';
import { Link } from "react-router-dom";

import '../../Styles/ListStyle.css'

//this should do it it just needs to display the data and then.
//then 
export default function ListEntry(props)
{
    return (
        //top level for the entry
        
        <Link to={props.workLocation}>            
            <div className="workEntry">
                <img src={props.imgSrc}></img>
                <div className="workInformation">                
                    <p className="title">{props.title}</p>
                    <p className="datePublished">{props.date}</p>
                </div>
            </div>
        </Link>
        
    )
}