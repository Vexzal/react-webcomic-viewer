//--dependencies--
import './Styles/App.css';
import React,{useState} from 'react'
import {BrowserRouter,Route,Link,Switch} from 'react-router-dom'


import ListView, {} from './Components/HomePage/ListView'
import ComicViewer from './Components/ComicViewer.js'

const data = require('./MOCK_DATA.json');
const comic = require('./COMIC_DATA.json');

//--Components--

//for now i'll just hard code.
//hard code what.
//well I'll need a list item but for now a page will have
//list items and list canvas.

function App() {
  
  const call = [<p>Entry</p>,<p>Second</p>,<p>third</p>]
  //const database = JSON.parse(data);


  return (
    <div className="App">
     
      <BrowserRouter>
        
        
        
        <switch>
          
          <Route exact={true} path="/">
            <h1> Header Hello</h1>
            <ListView databaseCollection={data}/>
          </Route>
          <Route path="/work/:workID/:pageNumber">
            <ComicViewer comic={comic}/>
          </Route>
          <Link className="homeButton" to="/">Home</Link>
        </switch>
        
      </BrowserRouter>

      

    </div>
  );
}

export default App;
